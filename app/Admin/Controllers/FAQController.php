<?php

namespace App\Admin\Controllers;

use App\FAQ;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class FAQController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'FAQ';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new FAQ);

        $grid->column('id', __('№'));
        $grid->column('question', __('Вопрос'));
        $grid->column('answer', __('Ответ'));
        $grid->column('created_at', __('Создано'));
        $grid->column('updated_at', __('Изменено'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(FAQ::findOrFail($id));

        $show->field('id', __('№'));
        $show->field('question', __('Вопрос'));
        $show->field('answer', __('Ответ'));
        $show->field('created_at', __('Создано'));
        $show->field('updated_at', __('Изменено'));


        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new FAQ);
        $form->text('question', __('Вопрос'));
        $form->textarea('answer', __('Ответ'))->rows(3);


        return $form;
    }
}
