<?php

namespace App\Admin\Controllers;

use App\UPhoto;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use App\Spec;

class UPhotoController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'App\UPhoto';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new UPhoto);

        $grid->column('id', __('№'));
        $grid->column('spec', __('Специализация'));
        $grid->column('photo',__('Фото'));
        $grid->column('created_at', __('Создано'));
        $grid->column('updated_at', __('Изменено'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(UPhoto::findOrFail($id));

        $show->field('id', __('№'));
        $show->field('spec', __('Специализация'));
        $show->field('photo',__('Фото'));
        $show->field('created_at', __('Создано'));
        $show->field('updated_at', __('Изменено'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new UPhoto);

        $form->select('spec', __('Специализация'))->options($this->getSpecs());
        $form->image('photo',__('Фото'));

        return $form;
    }

    private function getSpecs() 
    {
        $specs = Spec::get();
        $response = [];
        foreach($specs as $row) {
            $response[$row->id] = $row->name;
        }
        return $response;
    }
}
